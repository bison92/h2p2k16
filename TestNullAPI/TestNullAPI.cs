﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NullAPI.Controllers;
using NullAPI.Repository;
using System.Collections.Generic;
using NullAPI.Models;

namespace NullAPITest
{
    [TestClass]
    public class TestNullAPI
    {
        ValuesController sut;

        ComercioRepository repo;

        [TestInitialize]
        public void TestInitialize()
        {
            sut = new ValuesController();
            repo = new ComercioRepository();
        }
        [TestMethod]
        public void TestPostValue()
        {
            int res = sut.Post("value22");
            Assert.AreEqual("value22", sut.Get(res));
        }

        [TestMethod]
        public void TestRepoGetAll()
        {
            List<Comercio> resultado = repo.Get();
            Assert.IsNotNull(resultado);
            Assert.AreNotEqual(0, resultado.Count);
        }

        [TestMethod]
        public void TestRepoGet()
        {
            Comercio resultado = repo.Get("5838ff236ef94104504ab69c");
            Assert.IsNotNull(resultado);
        }

        [TestMethod]
        public void TestRepoGetTipos()
        {
            var resultado = repo.GetTipos();
            Assert.IsNotNull(resultado);
        }

        [TestMethod]
        public void TestRepoGetBarrios()
        {
            var resultado = repo.GetBarrios();
            Assert.IsNotNull(resultado);
            Assert.AreNotEqual(0, resultado.Count);
        }

        [TestMethod]
        public void TestRepoGetCoutn()
        {
            var resultado = repo.GetCountPorBarrio("LLIBRES, DIARIS I REVISTES");
            Assert.IsNotNull(resultado);
            Assert.AreNotEqual(0, resultado.Count);
        }
    }
}
