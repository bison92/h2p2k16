﻿using NullAPI.Models;
using NullAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NullAPI.Controllers
{
    public class TiposController : ApiController
    {
        public ComercioRepository repo = new ComercioRepository();

        // GET api/Comercios
        public List<String> Get()
        {
            return repo.GetTipos();
        }
    }
}
