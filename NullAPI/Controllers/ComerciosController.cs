﻿using NullAPI.Models;
using NullAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NullAPI.Controllers
{
    public class ComerciosController : ApiController
    {
        public ComercioRepository repo = new ComercioRepository();

        // GET api/Comercios
        public IEnumerable<Comercio> Get()
        {
            return repo.Get();
        }

        // GET api/Comercios
        public Dictionary<String, long> Get(String id)
        {
            return repo.GetCountPorBarrio(id);
        }
    }
}
