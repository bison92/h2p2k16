﻿using NullAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NullAPI.Controllers
{
    public class ValuesController : ApiController
    {
        public IValueRepository repo = new ValueRepository();
        // GET api/values
        public IEnumerable<string> Get()
        {
            return repo.Get().Select(v => v.Value);
        }

        // GET api/values/5
        public string Get(int id)
        {
            return repo.Get(id).Value;
        }

        // POST api/values
        public int Post([FromBody]string value)
        {
            return repo.Post(new ValueEntity(value)).Id;
        }

        // PUT api/values/5
        public string Put(int id, [FromBody]string value)
        {
            return repo.Put(new ValueEntity(value) { Id = id }).Value;
        }

        // DELETE api/values/5
        public string Delete(int id)
        {
            return repo.Delete(id).Value;
        }
    }
}
