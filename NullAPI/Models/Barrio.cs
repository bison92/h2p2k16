﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NullAPI.Models
{
    [BsonIgnoreExtraElements]
    public class Barrio
    {
        [BsonElement("BARRIS")]
        public String Nombre { get; set; }
        [BsonElement("TOTAL")]
        public int Poblacion { get; set; }
    }
}