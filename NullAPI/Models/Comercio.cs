﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NullAPI.Models
{
    [BsonIgnoreExtraElements]
    public class Comercio
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("NOM")]
        private String nombre { get; set; }

        [BsonElement("ï»¿CODI_EIXOS")]
        private String CODI_EIXOS { get; set; }
        [BsonElement("M_CODI_EIXOS")]
        private String M_CODI_EIXOS { get; set; }

//        [BsonElement("BCN1")]
//        private String tipo1 { get; set; }
//        [BsonElement("BCN2")]
//        private String tipo2 { get; set; }
        [BsonElement("BCN3")]
        private String tipo3 { get; set; }

        [BsonElement("CBARRI")]
        private int codBarrio { get; set; }
        [BsonElement("NBARRI")]
        private String barrio { get; set; }

        [BsonElement("CDISTRI")]
        private int codDistrito { get; set; }
        [BsonElement("NDISTRIC")]
        private String distrito { get; set; }

        [BsonElement("CARRCADAST")]
        private String CARRCADAST { get; set; }
        [BsonElement("DOORNUM")]
        private String DOORNUM { get; set; }

        [BsonElement("X_50ED")]
        private String lat { get; set; }

        [BsonElement("Y_50ED")]
        private String lng { get; set; }
    }
}