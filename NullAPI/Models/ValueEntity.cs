﻿namespace NullAPI
{
    public class ValueEntity
    {
        public ValueEntity()
        {
        }
        public ValueEntity(string value)
        {
            this.Value = value;
        }
        public int Id { get; set; }
        public string Value { get; set; }
    }
}