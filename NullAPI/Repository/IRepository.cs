﻿using System.Collections.Generic;

namespace NullAPI.Repository
{
    public interface IRepository<T>
    {
        T Post(T entity);
        IEnumerable<T> Get();
        T Get(int id);
        T Put(T entity);
        T Delete(int id);
    }
}