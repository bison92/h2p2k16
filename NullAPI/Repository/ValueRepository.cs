﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NullAPI.Repository
{
    public class ValueRepository : IValueRepository
    {

        public IEnumerable<ValueEntity> Get()
        {
            using (var ctx = new NullDB())
            {
                return ctx.Values.AsEnumerable();
            }
        }

        public ValueEntity Get(int id)
        {
            if (id > 0)
            {
                using (var ctx = new NullDB())
                {
                    return ctx.Values.Where(v => v.Id == id).SingleOrDefault();
                }
            }
            else
            {
                return null;
            }
        }

        public ValueEntity Post(ValueEntity entity)
        {
            if (entity != null)
            {
                using (var ctx = new NullDB())
                {
                    ValueEntity res = ctx.Values.Add(entity);
                    ctx.SaveChanges();
                    return res;
                }
            }
            else
            {
                return null;
            }
        }

        public ValueEntity Put(ValueEntity entity)
        {
            using(var ctx = new NullDB())
            {
                ValueEntity original = ctx.Values.Where(v => v.Id == entity.Id).SingleOrDefault();
                if(original != null)
                {
                    original.Value = entity.Value;
                    ctx.SaveChanges();
                    return original;
                }
                else
                {
                    return null;
                }
            }
        }

        public ValueEntity Delete(int id)
        {
            using (var ctx = new NullDB())
            {
                ValueEntity val = ctx.Values.Where(v => v.Id == id).SingleOrDefault();
                if ((val) != null)
                {
                    ctx.Values.Remove(val);
                    ctx.SaveChanges();
                }
                return val;
            }
        }

    }
}