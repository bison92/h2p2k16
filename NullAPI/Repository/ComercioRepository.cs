﻿using NullAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using NullAPI.Models;
using System.Threading;

namespace NullAPI.Repository
{
    public class ComercioRepository
    {
        protected static IMongoClient _client;
        protected static IMongoDatabase _database;

        public ComercioRepository()
        {
            var settings = new MongoClientSettings();
            settings.Server = new MongoServerAddress("192.168.137.1");
            _client = new MongoClient(settings);
            _database = _client.GetDatabase("equipo");
        }

        //public IEnumerable<T> Get()
        public List<Comercio> Get()
        {
            //var bsoncollection = _database.GetCollection<BsonDocument>("comercios");
            //var listBson = bsoncollection.Find(new BsonDocument()).ToList();

            IMongoCollection<Comercio> collection = _database.GetCollection<Comercio>("comercios");
            var list = collection.Find(new BsonDocument()).ToList();
            return list;
        }

        public Comercio Get(String id)
        {
            IMongoCollection<Comercio> collection = _database.GetCollection<Comercio>("comercios");

            var filter = new BsonDocument() {
                {  "_id", new ObjectId(id) }
            };
            Comercio comercio = collection.Find(filter).Single();
            return comercio; 
        }

        public List<String> GetTipos()
        {
            List<String> result = null;

            IMongoCollection<BsonDocument> collection = _database.GetCollection<BsonDocument>("comercios");
            var prueba = collection.Distinct<String>("BCN3", new BsonDocument(), new DistinctOptions(), new CancellationToken());
            result = prueba.ToList();

            return result;
        }

        public List<Barrio> GetBarrios()
        {
            List<Barrio> result = null;

            IMongoCollection<Barrio> collection = _database.GetCollection<Barrio>("barrios_poblacion");
            result = collection.Find(new BsonDocument()).ToList();

            return result;
        }

        public Dictionary<String, long> GetCountPorBarrio(String servicio)
        {
            IMongoCollection<Barrio> barriosCollection = _database.GetCollection<Barrio>("barrios_poblacion");
            IMongoCollection<BsonDocument> collection = _database.GetCollection<BsonDocument>("comercios");

            var listBarrios = GetBarrios();
            var result = new Dictionary<String, long>();

            foreach(Barrio barri in listBarrios)
            {

                var filter = new BsonDocument()
                {
                    {  "BCN3", servicio },
                    {  "NBARRI", barri.Nombre }
                };

                var count = collection.Find(filter).Count();
                result.Add(barri.Nombre, count);
            }

            return result;

        }

    }
}