﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NullAPI
{
    public class NullDB : DbContext
    {
        public NullDB() : base()
        {

        }
        public DbSet<ValueEntity> Values { get; set; }
    }
}